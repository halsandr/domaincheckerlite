use std::io;
use crate::domain::Domain;
use crate::import::get_config;
use crate::cert::populate_domains;

mod domain;
mod import;
mod cert;

fn main() {
    // Returns a Vector of Domain structs, parsed from a config file
    let mut conf: Vec<Domain> = get_config().unwrap_or_default();

    // Populates the properties in the Domain object
    populate_domains(&mut conf);

    // Prints the results
    for dom in conf {
        println!("{dom}");
    }

    println!("\r\nHit the any key to exit!");

    if cfg!(not(debug_assertions)) {
        io::stdin().read_line(&mut String::new()).unwrap();
    }
}
