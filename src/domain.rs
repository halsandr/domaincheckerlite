use url::{Url, ParseError};
use reqwest::StatusCode;
use std::fmt;
use chrono::Duration;
use colored::{Colorize, ColoredString};

#[derive(Debug)]
pub struct Domain {
    pub domain: Url,
    pub status: Option<StatusCode>,
    pub tte: Option<i64>,
    pub brand: Option<String>
}

impl Domain {
    // Created a new instance from just a url
    pub fn new(in_domain: &str) -> Result<Domain, ParseError> {
        let out_domain: String;
        if !in_domain.starts_with("http") {
            // Adds the protocol if it isn't populated
            out_domain = format!("https://{}", in_domain);
        } else {
            out_domain = String::from(in_domain);
        }

        Ok(Domain {
            domain: Url::parse(out_domain.as_str())?,
            status: None,
            tte: None,
            brand: None
        })
    }
}

// Implements Display formatting for use in printing result
impl fmt::Display for Domain {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        let domain: ColoredString;
        let status: ColoredString;
        let brand: ColoredString;
        let tte: ColoredString;

        // If we don't have a usable value, this is default, in red
        let unknown: ColoredString = String::from("???").red();

        // If we don't have a usable status, set it as SERVICE_UNAVAILABLE
        let status_get: StatusCode = self.status.unwrap_or(StatusCode::SERVICE_UNAVAILABLE);

        // 
        // domain = self.domain.host_str().unwrap_or(&unknown).into();
        domain = format!("{}://{}", self.domain.scheme(), self.domain.host_str().unwrap_or(&unknown)).into();

        if status_get.is_success() {
            status = status_get.as_u16().to_string().green();
        } else {
            status = status_get.as_u16().to_string().red();
        }

        match self.brand.as_ref() {
            Some(value) => brand = value.as_str().green(),
            None => brand = unknown.clone()
        }

        tte=match self.tte {
            Some(s) => {
                let duration = Duration::seconds(s);

                if duration.num_days() < 15 {
                    duration.num_days().to_string().red()
                } else if duration.num_days() < 30 {
                    duration.num_days().to_string().yellow()
                } else {
                    duration.num_days().to_string().green()
                }
            },
            None => unknown.clone()
        };
        
        write!(f, "{domain} => status: {status}, brand: {brand}, TTE(days): {tte}")
    }
}