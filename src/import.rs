use crate::domain::Domain;
use std::fs::File;
use std::io::{self, BufRead};

const DOMAIN_FILE: &'static str = "./domains.txt";

pub fn get_config() -> Option<Vec<Domain>> {
    let mut out: Vec<Domain> = Vec::new();
    let file: File;

    // Tries to open the domain file
    match File::open(DOMAIN_FILE) {
        Ok(f) => file = f,
        Err(_) => {
            eprintln!("Error opening {DOMAIN_FILE}");
            return None
        }
    }

    let lines = io::BufReader::new(file).lines();

    // Creates new Domain structs from each line in hte config
    for line in lines {
        match line {
            Ok(l) => {
                if 
                    l.trim().starts_with("#") || 
                    l.trim().starts_with("//") || 
                    l.trim().is_empty()
                {
                    continue
                }

                match Domain::new(& l.trim()) {
                    Ok(d) => out.push(d),
                    Err(_) => eprint!("Unable to parse url: {l}")
                };
            },
            Err(_) => eprintln!("Unable to read line")
        }
    }
    
    Some(out)
}